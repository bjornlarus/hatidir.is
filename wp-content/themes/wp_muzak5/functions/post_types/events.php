<?php
//
// events post type related functions.
//
add_action( 'init', 'ci_create_cpt_events' );

if( !function_exists('ci_create_cpt_events') ):
function ci_create_cpt_events()
{
	$labels = array(
		'name' => _x('Events', 'post type general name', 'ci_theme'),
		'singular_name' => _x('Events Item', 'post type singular name', 'ci_theme'),
		'add_new' => __('New Events Item', 'ci_theme'),
		'add_new_item' => __('Add New Events Item', 'ci_theme'),
		'edit_item' => __('Edit Events Item', 'ci_theme'),
		'new_item' => __('New Events Item', 'ci_theme'),
		'view_item' => __('View Events Item', 'ci_theme'),
		'search_items' => __('Search Events Items', 'ci_theme'),
		'not_found' =>  __('No Events Items found', 'ci_theme'),
		'not_found_in_trash' => __('No Events Items found in the trash', 'ci_theme'), 
		'parent_item_colon' => __('Parent Events Item:', 'ci_theme')
	);

	$args = array(
		'labels' => $labels,
		'singular_label' => __('Events Item', 'ci_theme'),
		'public' => true,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'has_archive' => false,
		'rewrite' => array('slug' => 'events'),
		'menu_position' => 5,
		'supports' => array('title', 'editor', 'thumbnail')		
	);

	register_post_type( 'cpt_events' , $args );

}
endif;

add_action( 'load-post.php', 'events_meta_boxes_setup' );
add_action( 'load-post-new.php', 'events_meta_boxes_setup' ); 

if( !function_exists('events_meta_boxes_setup') ):
function events_meta_boxes_setup() {
	add_action( 'add_meta_boxes', 'events_add_meta_boxes' );
	add_action( 'save_post', 'events_save_meta', 10, 2 );
}
endif;

if( !function_exists('events_add_meta_boxes') ):
function events_add_meta_boxes() {
	add_meta_box( 'events-box', __( 'Events Settings', 'ci_theme' ), 'events_score_meta_box', 'cpt_events', 'normal', 'high' );
}
endif;

if( !function_exists('events_score_meta_box') ):
function events_score_meta_box( $object, $box )
{
	wp_nonce_field( basename( __FILE__ ), 'events_nonce' );
	?>
	<div class="postbox" style="margin-top:20px">
		<h3><?php _e('Event details', 'ci_theme'); ?></h3>
		<div class="inside">
			<?php
				$recurrent = get_post_meta($object->ID, 'ci_cpt_event_recurrent', true);
				$recurrence = get_post_meta($object->ID, 'ci_cpt_event_recurrence', true);
			?>
			<p>
				<label for="ci_cpt_events_venue"><?php _e( 'Event venue', 'ci_theme' ); ?></label>
				<input type="text" name="ci_cpt_events_venue" class="widefat" value="<?php echo esc_attr( get_post_meta( $object->ID, 'ci_cpt_events_venue', true ) ); ?>" />
			</p>
			<p>
				<label for="ci_cpt_events_location"><?php _e( 'Event Location', 'ci_theme' ); ?></label>
				<input type="text" name="ci_cpt_events_location" class="widefat" value="<?php echo esc_attr( get_post_meta( $object->ID, 'ci_cpt_events_location', true ) ); ?>" />
			</p>

			<p>
				<input id="ci_event_recurrent" name="ci_cpt_event_recurrent" type="checkbox" value="enabled" <?php checked('enabled', $recurrent); ?> />
				<label for="ci_event_recurrent"><?php _e('Recurrent Event', 'ci_theme'); ?></label>
			</p>

			<p id="event_recurrent">
				<label for="ci_event_recurrence"><?php _e('Event Recurrence. You may use &lt;span class="date"> and &lt;span class="time"> to denote dates and times respectively. (e.g. <b>Every &lt;span class="date">TUE&lt;/span> &lt;span class="time">22:00&lt;/span></b>)', 'ci_theme'); ?></label>
				<input id="ci_event_recurrence" name="ci_cpt_event_recurrence" type="text" class="widefat" value="<?php echo esc_attr($recurrence); ?>" />
			</p>

			<p id="event_datetime">
				<label for="ci_cpt_events_date"><?php _e( 'Date from', 'ci_theme' ); ?></label>
				<input type="text" id="ci_cpt_events_date" class="widefat" name="ci_cpt_events_date" value="<?php echo esc_attr( get_post_meta( $object->ID, 'ci_cpt_events_date', true ) ); ?>" />

				<label for="ci_cpt_events_date_to"><?php _e( 'Date to', 'ci_theme' ); ?></label>	
				<input type="text" id="ci_cpt_events_date_to" class="widefat" name="ci_cpt_events_date_to" value="<?php echo esc_attr( get_post_meta( $object->ID, 'ci_cpt_events_date_to', true ) ); ?>" />

				<!-- <label for="ci_cpt_events_time"><?php _e('Time', 'ci_theme'); ?></label>
				<input id="ci_cpt_events_time" class="widefat" name="ci_cpt_events_time" type="text" value="<?php echo esc_attr( get_post_meta( $object->ID, 'ci_cpt_events_time', true ) ); ?>" /> -->
			</p>	

			<p>
				<label for="ci_cpt_more_info_url"><?php _e('More info (URL)', 'ci_theme'); ?></label>
				<input id="ci_cpt_more_info_url" class="widefat" name="ci_cpt_more_info_url" type="text" value="<?php echo esc_attr( get_post_meta( $object->ID, 'ci_cpt_more_info_url', true ) ); ?>" />
			</p>		
		</div><!-- /inside -->
	</div><!-- /postbox -->
	
	<div class="postbox" style="margin-top:20px">
		<h3><?php _e('Event status', 'ci_theme'); ?></h3>
		<div class="inside">
			<p>
				<label for="ci_cpt_events_status"><?php _e( 'Select the event status.', 'ci_theme' ); ?></label><br>
				<select name="ci_cpt_events_status">
					<?php $event_status = get_post_meta( $object->ID, 'ci_cpt_events_status', true ); ?>
					<option value="" <?php selected('', $event_status); ?>>&nbsp;</option>
					<option value="buy" <?php selected('buy', $event_status); ?>><?php _e('Tickets Available','ci_theme'); ?></option>
					<option value="sold" <?php selected('sold', $event_status); ?>><?php _e('Sold Out','ci_theme'); ?></option>
					<option value="canceled" <?php selected('canceled', $event_status); ?>><?php _e('Cancelled','ci_theme'); ?></option>
					<option value="watch" <?php selected('watch', $event_status); ?>><?php _e('Watch Live','ci_theme'); ?></option>
				</select>				
			</p>
			<p>
				<label for="ci_cpt_events_button"><?php _e( 'Set the button text like "Buy now" or "Join us" etc', 'ci_theme' ); ?></label>
				<input type="text" name="ci_cpt_events_button" class="widefat" value="<?php echo esc_attr( get_post_meta( $object->ID, 'ci_cpt_events_button', true ) ); ?>" />
			</p>
			<p>
				<label for="ci_cpt_events_tickets"><?php _e( 'If the event is still available enter the URL where someone can buy tickets.', 'ci_theme' ); ?></label>
				<input type="text" name="ci_cpt_events_tickets" class="widefat" value="<?php echo esc_attr( get_post_meta( $object->ID, 'ci_cpt_events_tickets', true ) ); ?>" size="30" />
			</p>			
			<p>
				<label for="ci_cpt_events_live"><?php _e( 'Is there a live streaming available from the event? Enter it here.', 'ci_theme' ); ?></label>
				<input type="text" name="ci_cpt_events_live" class="widefat" value="<?php echo esc_attr( get_post_meta( $object->ID, 'ci_cpt_events_live', true ) ); ?>" />
			</p>
		</div><!-- /inside -->
	</div><!-- /postbox -->
	
	<div class="postbox" style="margin-top:20px">
		<h3><?php _e('Social URL', 'ci_theme'); ?></h3>
		<div class="inside">
			<p>
				<label for="ci_cpt_facebook"><?php _e( 'Facebook URL', 'ci_theme' ); ?></label>
				<input type="text" name="ci_cpt_facebook" class="widefat" value="<?php echo esc_attr( get_post_meta( $object->ID, 'ci_cpt_facebook', true ) ); ?>" size="30" />
			</p>
			<p>
				<label for="ci_cpt_twitter"><?php _e( 'Twitter URL', 'ci_theme' ); ?></label>
				<input type="text" name="ci_cpt_twitter" class="widefat" value="<?php echo esc_attr( get_post_meta( $object->ID, 'ci_cpt_twitter', true ) ); ?>" size="30" />
			</p>
			<p>
				<label for="ci_cpt_instagram"><?php _e( 'Instagram URL', 'ci_theme' ); ?></label>
				<input type="text" name="ci_cpt_instagram" class="widefat" value="<?php echo esc_attr( get_post_meta( $object->ID, 'ci_cpt_instagram', true ) ); ?>" size="30" />
			</p>
		</div><!-- /inside -->
	</div><!-- /postbox -->

	<div class="postbox" style="margin-top:20px">
		<h3><?php _e('Event Map', 'ci_theme'); ?></h3>
		<div class="inside">
			<p>
				<label for="ci_cpt_events_lat"><?php _e( 'Location Latitude.', 'ci_theme' ); ?></label>
				<input type="text" name="ci_cpt_events_lat" class="widefat" value="<?php echo esc_attr( get_post_meta( $object->ID, 'ci_cpt_events_lat', true ) ); ?>" />
			</p>
			<p>
				<label for="ci_cpt_events_lon"><?php _e( 'Location Longtitude.', 'ci_theme' ); ?></label>
				<input type="text" name="ci_cpt_events_lon" class="widefat" value="<?php echo esc_attr( get_post_meta( $object->ID, 'ci_cpt_events_lon', true ) ); ?>" />
			</p>
		</div><!-- /inside -->
	</div><!-- /postbox -->
	<?php 
}
endif;

if( !function_exists('events_save_meta') ):
function events_save_meta( $post_id, $post ) {
	
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
	if ( !isset( $_POST['events_nonce'] ) || !wp_verify_nonce( $_POST['events_nonce'], basename( __FILE__ ) ) ) return $post_id;
	if (isset($_POST['post_view']) and $_POST['post_view']=='list') return;

	$post_type = get_post_type_object( $post->post_type );
	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
		return $post_id;
		

	update_post_meta($post_id, "ci_cpt_event_recurrent", (isset($_POST["ci_cpt_event_recurrent"]) ? $_POST["ci_cpt_event_recurrent"] : '') );
	update_post_meta($post_id, "ci_cpt_event_recurrence", (isset($_POST["ci_cpt_event_recurrence"]) ? $_POST["ci_cpt_event_recurrence"] : '') );

	if(isset($_POST["ci_cpt_event_recurrent"]) and $_POST["ci_cpt_event_recurrent"]=='enabled' )
	{
		// Since it's a recurring event, we need to delete date and time information, so 
		// that it won't interfere with wp_query queries.
		//update_post_meta($post_id, "ci_cpt_events_date", '');
		//update_post_meta($post_id, "ci_cpt_events_time", '');
		delete_post_meta($post_id, "ci_cpt_events_date");
		delete_post_meta($post_id, "ci_cpt_events_date_to");
		delete_post_meta($post_id, "ci_cpt_events_time");
	}
	else
	{
		update_post_meta($post_id, "ci_cpt_events_date", (isset($_POST["ci_cpt_events_date"]) ? $_POST["ci_cpt_events_date"] : '') );
		update_post_meta($post_id, "ci_cpt_events_date_to", (isset($_POST["ci_cpt_events_date_to"]) ? $_POST["ci_cpt_events_date_to"] : '') );
		update_post_meta($post_id, "ci_cpt_events_time", (isset($_POST["ci_cpt_events_time"]) ? $_POST["ci_cpt_events_time"] : '') );
	}		

	//update_post_meta($post->ID, "ci_cpt_events_date", (isset($_POST["ci_cpt_events_date"]) ? $_POST["ci_cpt_events_date"] : '') );
	//update_post_meta($post->ID, "ci_cpt_events_time", (isset($_POST["ci_cpt_events_time"]) ? $_POST["ci_cpt_events_time"] : '') );


	update_post_meta($post->ID, "ci_cpt_events_venue", (isset($_POST["ci_cpt_events_venue"]) ? $_POST["ci_cpt_events_venue"] : '') );
	update_post_meta($post->ID, "ci_cpt_events_location", (isset($_POST["ci_cpt_events_location"]) ? $_POST["ci_cpt_events_location"] : '') );
	update_post_meta($post->ID, "ci_cpt_events_status", (isset($_POST["ci_cpt_events_status"]) ? $_POST["ci_cpt_events_status"] : '') );
	update_post_meta($post->ID, "ci_cpt_events_tickets", (isset($_POST["ci_cpt_events_tickets"]) ? $_POST["ci_cpt_events_tickets"] : '') );
	update_post_meta($post->ID, "ci_cpt_events_button", (isset($_POST["ci_cpt_events_button"]) ? $_POST["ci_cpt_events_button"] : '') );
	update_post_meta($post->ID, "ci_cpt_events_live", (isset($_POST["ci_cpt_events_live"]) ? $_POST["ci_cpt_events_live"] : '') );
	update_post_meta($post->ID, "ci_cpt_events_lon", (isset($_POST["ci_cpt_events_lon"]) ? $_POST["ci_cpt_events_lon"] : '') );
	update_post_meta($post->ID, "ci_cpt_events_lat", (isset($_POST["ci_cpt_events_lat"]) ? $_POST["ci_cpt_events_lat"] : '') );
	update_post_meta($post->ID, "ci_cpt_more_info_url", (isset($_POST["ci_cpt_more_info_url"]) ? $_POST["ci_cpt_more_info_url"] : '') );


	update_post_meta($post->ID, "ci_cpt_facebook", (isset($_POST["ci_cpt_facebook"]) ? $_POST["ci_cpt_facebook"] : '') );
	update_post_meta($post->ID, "ci_cpt_twitter", (isset($_POST["ci_cpt_twitter"]) ? $_POST["ci_cpt_twitter"] : '') );
	update_post_meta($post->ID, "ci_cpt_instagram", (isset($_POST["ci_cpt_instagram"]) ? $_POST["ci_cpt_instagram"] : '') );

}
endif;

//
// Event post type custom admin list
//
add_filter('manage_edit-cpt_events_columns', 'ci_cpt_event_edit_columns');
add_action('manage_posts_custom_column',  'ci_cpt_event_custom_columns');

if( !function_exists('ci_cpt_event_edit_columns') ):
function ci_cpt_event_edit_columns($columns)
{
	$new_columns = array(
		"cb" => $columns['cb'],
		"title" => __('Event Name', 'ci_theme'),
		"event_location" => __("Event location", 'ci_theme'),
		"event_date" => __("Event Date", 'ci_theme'),
		"date" => $columns['date']
	);

	return $new_columns;
}
endif;

if( !function_exists('ci_cpt_event_custom_columns') ):
function ci_cpt_event_custom_columns($column)
{
	global $post;

	switch ($column)
	{
		case "event_location":
			echo get_post_meta($post->ID, 'ci_cpt_events_location', true);
			break;
		case "event_date":
			echo get_post_meta($post->ID, 'ci_cpt_events_date', true);
			echo get_post_meta($post->ID, 'ci_cpt_events_date_to', true);
			break;
		
	}
}
endif;

if( !function_exists('printable_event_cpt_datetime') ):
function printable_event_cpt_datetime()
{
	global $post;

	$recurrent = get_post_meta($post->ID, 'ci_cpt_event_recurrent', true)=='enabled' ? true : false;
	$recurrence = get_post_meta($post->ID, 'ci_cpt_event_recurrence', true);
	$event_date = get_post_meta( $post->ID, 'ci_cpt_events_date', true );
	$event_date = get_post_meta( $post->ID, 'ci_cpt_events_date_to', true );
	$event_time = get_post_meta( $post->ID, 'ci_cpt_events_time', true );

	if($recurrent)
	{
		return $recurrence;
	}
	else
	{
		$dt = new DateTime();
		$dt->setDate( substr( $event_date, 0, 4 ), substr( $event_date, 5, 2 ), substr( $event_date, 8, 2 ) );
		$dt->setTime( substr( $event_time, 0, 2 ), substr( $event_time, 3, 2 ) );
		$datetime = date_i18n( get_option('date_format').' - '.get_option('time_format'), strtotime($dt->format( 'F j Y, H:i' )) );
		return $datetime;	
	}

}
endif;

if( !function_exists('attribute_event_cpt_datetime') ):
function attribute_event_cpt_datetime()
{
	global $post;

	$recurrent = get_post_meta($post->ID, 'ci_cpt_event_recurrent', true)=='enabled' ? true : false;
	$event_date = get_post_meta( $post->ID, 'ci_cpt_events_date', true );
	$event_date = get_post_meta( $post->ID, 'ci_cpt_events_date_to', true );
	$event_time = get_post_meta( $post->ID, 'ci_cpt_events_time', true );

	if($recurrent)
	{
		return '';
	}
	else
	{
		return $event_date . ' ' . $event_time;
	}

}
endif;

?>